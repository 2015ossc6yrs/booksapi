#!/usr/bin/ruby

require 'mongo'
require 'json'

client = Mongo::Client.new([ '127.0.0.1:27017' ], :database => 'bookstore')

books = client[:books].find(:parentGenreId => '001001001')
res = []
books.each do |item|
 res << {
  isbn: item['isbn'], size: item['size'],
  #smallImageUrl: item['smallImageUrl'], largeImageUrl: item['largeImageUrl']
  smallImageUrl: item['smallImageUrl']
 } unless item['smallImageUrl'].index('noimage')
 break if res.length > 90
end

puts res.to_json
