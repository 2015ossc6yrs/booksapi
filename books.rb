require 'sinatra'
require 'json'
require 'rakuten_web_service'
require 'mongo'

RakutenWebService.configuration do |c|
  c.application_id = '1010355418644878299'
  c.affiliate_id = '142452b2.e02c6c39.142452b3.db12a911'
end

client = Mongo::Client.new([ '127.0.0.1:27017' ], :database => 'bookstore')

# isbnで検索
get '/books/:isbn' do
 isbn = params['isbn']
 books = RakutenWebService::Books::Book.search(:isbn => isbn)
 res = []
 books.first(10).each do |item|
  res << {
   isbn: item['isbn'], title: item['title'], subTitle: item['subTitle'], 
   seriesName: item['seriesName'], author: item['author'], publisherName: item['publisherName'], 
   size: item['size'], itemCaption: item['itemCaption'], salesDate: item['salesDate'],
   itemPrice: item['itemPrice'], itemUrl: item['itemUrl'],
   smallImageUrl: item['smallImageUrl'], largeImageUrl: item['largeImageUrl'],
   reviewCount: item['reviewCount'], reviewAverage: item['reviewAverage'],
   booksGenreId: item['booksGenreId']
  }
 end
 res.to_json
end 

# ジャンルで取得
get '/books' do
 genre = params['genre']
 pass unless genre
 size = params['size'] || '10'
 #books = RakutenWebService::Books::Book.search(:booksGenreId => genre)
 books = client[:books].find(:parentGenreId => genre)
 res = []
 books.each do |item|
  res << {
   isbn: item['isbn'], size: item['size'], 
   #smallImageUrl: item['smallImageUrl'], largeImageUrl: item['largeImageUrl']
   #smallImageUrl: item['smallImageUrl']
   smallImageUrl: 'http://183.181.30.92:4567/9784757547049.jpg'
  } unless item['smallImageUrl'].index('noimage')
  break if res.length > size.to_i
 end
 #send_file 'result.json'
 res.to_json
end 

# キーワードで取得
get '/books' do
 keyword = params['keyword']
 pass unless keyword
 size = params['size'] || '10'
 books = RakutenWebService::Books::Total.search(:keyword => keyword, :sort => 'sales', :booksGenreId => '001')
 res = []
 books.each do |item|
  res << { title: item['title'],
   isbn: item['isbn'], size: item['size'], 
   smallImageUrl: item['smallImageUrl'], largeImageUrl: item['largeImageUrl']
  } unless item['smallImageUrl'].index('noimage')
  break if res.length > size.to_i
 end
 res.to_json
end 

# 全ての本
get '/books' do
 size = params['size'] || '5000'
 books = client[:books].find()
 res = []
 books.each do |item|
  res << {
   isbn: item['isbn'], size: item['size'],
   #smallImageUrl: item['smallImageUrl'], largeImageUrl: item['largeImageUrl']
   smallImageUrl: item['smallImageUrl'], parentGenreId: item['parentGenreId']
  } unless item['smallImageUrl'].index('noimage')
  break if res.length > size.to_i
 end
 res.to_json
end
